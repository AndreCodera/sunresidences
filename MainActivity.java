package iacademy.com.weekfour;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {

    private int quantity = 1;
    private int dprice = 180;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.weekfour);
    }
    public void displayPrice(int number){
        TextView price = (TextView)findViewById(R.id.PriceValue);
        price.setText(NumberFormat.getCurrencyInstance().format(number));
    }

    public void submitOrder(View view){
        displayPrice(quantity * dprice);
    }

    public void displayQuantity(int n){
        TextView quant = (TextView) findViewById(R.id.quantnum);
        quant.setText("" + n);
    }

    public void increment(View view){
        quantity++;
        displayQuantity(quantity);
    }

    public void decrement(View view){
        quantity--;
        displayQuantity(quantity);
    }
}
